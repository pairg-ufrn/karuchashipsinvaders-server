<?php

namespace Brickify\Answer;

use Brickify\XML\XMLTag;

class BrickifyXML implements ServiceAnswer{
    
    private $mainTag;
    
    public function __construct(XMLTag $mainTag){
        $this->mainTag = $mainTag;
    }
    
    public function pack() {
        echo '<?xml version="1.0" encoding="utf-8" ?>';
        echo utf8_encode($this->mainTag->dump());
    }

}

