<?php

namespace RoastPHP\Util\Encrypt;

interface EncryptedData {

    public function encrypt($data);
}
