<?php

namespace Karucha\Access\Top;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\Rules\GetRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Session\SessionManager;

class TopGlyphErrorService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new GetRequestRule(), 'TOP GLYPH ERROR must be triggered by a GET request')
            ),
            'parameters' => array(
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        
        $dbDriver = DBBrick::getDriver();
        $sessionManager = SessionManager::getInstance();
        $sessionManager->getSessionVariable('user_id');
    }

}
