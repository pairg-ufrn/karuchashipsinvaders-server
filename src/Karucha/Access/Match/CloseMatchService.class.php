<?php

namespace Karucha\Access\Match;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\MatchDAO;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\Match\MatchExistsRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Tables\MatchesTable;

class CloseMatchService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'CLOSE MATCH operation must be triggered by a POST request')
            ),
            'parameters' => array(
                'match_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'The field MATCH_ID must be an integer value'),
                        new ServiceFilter(new MatchExistsRule(), 'The given Match does not exists')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client')
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        $dbDriver = DBBrick::getDriver();
        $dao = new MatchDAO($dbDriver);

        $resultSet = $dao->get($parameters['match_id']);
        $resultSet->setField(MatchesTable::COLUMN_STATUS, 1);

        $affected = $dao->save($resultSet);
        if ($affected == 1) {
            $answer->setStatus(1);
            $answer->setMessages(array('Match successfully closed.'));
            $answer->setContents(array('match_id' => $parameters['match_id']));
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Error while closing the Match.'));
            $answer->setContents(array());
        }

        return $answer;
    }

}
