<?php

namespace Brickify\Operation;

abstract class LinkedAction{
    
    protected $child;
    protected $errorMessage;
    protected $generatedContents;
    protected $parameterRequired;
    protected $actionStatus;


    abstract public function runAction($runParameters = null);
    
    public function __construct($errorMessage, $parameterRequired = false){
        $this->errorMessage = $errorMessage;
        $this->parameterRequired = $parameterRequired;
        $this->actionStatus = -1;
    }
    
    public function getGeneratedContents(){
        return $this->generatedContents;
    }
    
    public function getChild(){
        return $this->child;
    }
    
    public function setChild(LinkedAction $child){
        $this->child = $child;
    }
    
    public function getErrorMessage(){
        return $this->errorMessage;
    }
    
    public function setErrorMessage($errorMessage){
        $this->errorMessage = $errorMessage;
    }
    
    public function getActionStatus(){
        return $this->actionStatus;
    }
    
    public function requiresParameters(){
        return $this->parameterRequired;
    }
}