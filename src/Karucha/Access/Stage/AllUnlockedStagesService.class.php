<?php

namespace Karucha\Access\Stage;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\UnlockedStageDAO;
use Karucha\Rules\GetRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Session\SessionManager;
use Karucha\Tables\UnlockedStagesTable;
use RoastPHP\Util\Sql\SqlCriteria;
use RoastPHP\Util\Sql\SqlFilter;

class AllUnlockedStagesService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new GetRequestRule(), 'ALL UNLOCKED STAGES must be triggered by a GET request')
            ),
            'parameters' => array(
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        $dbDriver = DBBrick::getDriver();
        $unlockedDAO = new UnlockedStageDAO($dbDriver);
        $sessManager = SessionManager::getInstance();
        $userId = $sessManager->getSessionVariable('user_id');

        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(UnlockedStagesTable::COLUMN_USER_ID, '=', $userId));

        $allSelected = $unlockedDAO->get($criteria);
        $unlockedStages = array();
        foreach ($allSelected as $singleSelected) {
            $unlockedStages[] = array(
                'stage_mode' => $singleSelected->getField(UnlockedStagesTable::COLUMN_STAGE_MODE),
                'stage_number' => $singleSelected->getField(UnlockedStagesTable::COLUMN_STAGE_NUMBER));
        }

        $answer->setMessages(array('All stages were loaded'));
        $answer->setContents(array('stages' => $unlockedStages));

        return $answer;
    }

}
