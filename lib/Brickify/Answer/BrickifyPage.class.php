<?php

namespace Brickify\Answer;

class BrickifyPage implements ServiceAnswer {

    private $page;

    public function __construct($localpage) {
        $pageFile = getcwd() . '/' . $localpage;
        if (is_file($pageFile) && is_readable($pageFile)) {
            $this->page = $pageFile;
        } else {
            $this->page = 'res/unavailable.html';
        }
    }

    public function pack() {
        include_once getcwd() . '/' . $this->page;
    }

}
