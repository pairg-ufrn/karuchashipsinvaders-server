<?php

namespace Brickify\Rules;

interface DataRule{
    /**
     * @return boolean Indicates if the filtered data is valid for the given filter
     */
    public function isValidRule($data = null);
    
}