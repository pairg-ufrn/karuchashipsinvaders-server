<?php

namespace Karucha\Rules\Session;

use Brickify\Rules\DataRule;
use Karucha\Session\SessionManager;

class SameClientRule implements DataRule{
    
    public function isValidRule($data = null) {
        $sessionManager = SessionManager::getInstance();
        $sessionManager->startSession($data);
        return ($sessionManager->getSessionVariable('client_addr') == filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
    }

}
