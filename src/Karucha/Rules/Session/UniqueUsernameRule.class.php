<?php

namespace Karucha\Rules\Session;

use Brickify\Database\DBBrick;
use Brickify\Rules\DataRule;
use Karucha\DAOs\UserCredentialsDAO;
use Karucha\Tables\UserCredentialsTable;
use RoastPHP\Util\Sql\SqlCriteria;
use RoastPHP\Util\Sql\SqlFilter;

class UniqueUsernameRule implements DataRule{
    
    public function isValidRule($data = null) {
        $dbDriver = DBBrick::getDriver();
        $userCredentialsDAO = new UserCredentialsDAO($dbDriver);
        
        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(UserCredentialsTable::COLUMN_USERNAME, '=', $data));
        $results = $userCredentialsDAO->get($criteria);
        return empty($results);
    }

}