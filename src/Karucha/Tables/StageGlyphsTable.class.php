<?php

namespace Karucha\Tables;

class StageGlyphsTable{
    
    const TABLE_NAME = 'stage_glyphs';
    
    const COLUMN_ID = 'id';
    const COLUMN_STAGE_ID = 'stage_id';
    const COLUMN_TYPE = 'glyph_type';
    const COLUMN_COLUMN = 'glyph_column';
    const COLUMN_ROW = 'glyph_row';
    const COLUMN_STATUS = 'status';
    
    public static $columns = array(
        self::COLUMN_ID,
        self::COLUMN_STAGE_ID,
        self::COLUMN_TYPE,
        self::COLUMN_COLUMN,
        self::COLUMN_ROW,
        self::COLUMN_STATUS,
    );
    
}