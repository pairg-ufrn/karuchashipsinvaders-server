<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\UnlockedStagesTable;
use PDO;

class UnlockedStageDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(UnlockedStagesTable::TABLE_NAME, $driver, true, false);
    }
    
}