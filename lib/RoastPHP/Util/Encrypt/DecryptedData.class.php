<?php

namespace RoastPHP\Util\Encrypt;

interface DecryptedData {

    public function decrypt($data);
}
