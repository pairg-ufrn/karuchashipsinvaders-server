<?php

namespace Karucha\Access\Match;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\ResultSet\ResultSetSlot;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\MatchDAO;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Session\SessionManager;
use Karucha\Tables\MatchesTable;

class OpenMatchService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'OPEN MATCH operation must be triggered by a POST request')
            ),
            'parameters' => array(
                'previous_match' => array(
                    'required' => false,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'The field PREVIOUS_MATCH requires an integer value')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        $databaseDriver = DBBrick::getDriver();
        $dao = new MatchDAO($databaseDriver);

        $sessionManager = SessionManager::getInstance();
        $userId = $sessionManager->getSessionVariable('user_id');

        $structure = array(
            MatchesTable::COLUMN_USER_ID => $userId,
            MatchesTable::COLUMN_PREVIOUS_MATCH => (isset($parameters['previous_match']) ? $parameters['previous_match'] : 0)
        );

        $match = new ResultSetSlot($structure);
        $dao->save($match);

        $answer->setStatus(1);
        $answer->addMessage('A new match has been created');
        $answer->setContents(array('match_id' => $match->getField(MatchesTable::COLUMN_ID)));

        return $answer;
    }

}
