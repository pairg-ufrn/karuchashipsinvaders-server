<?php

namespace Brickify\Services;

use Brickify\Answer\BrickifyAnswer;

class BrickifyEmptyService extends BrickifyService{
    
    protected function serviceConditions() {
        return array();
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $answer->setStatus(0);
        $answer->addMessage("Invalid or unknow method");
        
        return $answer;
    }

}