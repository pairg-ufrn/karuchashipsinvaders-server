<?php

namespace Brickify\Services;

use Brickify\Rules\DataRule;

class ServiceFilter{

    private $rule;
    private $errorMessage;
    private $critical;

    public function __construct(DataRule $rule, $errorMessage, $critical = true) {
        $this->rule = $rule;
        $this->errorMessage = $errorMessage;
        $this->critical = $critical;
    }

    public function getRule() {
        return $this->rule;
    }

    public function getErrorMessage() {
        return $this->errorMessage;
    }

    public function isCritical() {
        return $this->critical;
    }

}
