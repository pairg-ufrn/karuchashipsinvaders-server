<?php

namespace Karucha\Access\Session;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\Rules\NoSpecialCharsRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\StringLengthFilter;
use Karucha\Session\PasswordAuthentication;
use Karucha\Session\SessionManager;
use Karucha\Tables\UserCredentialsTable;

class LoginService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'LOGIN service must be triggered by a HTTP POST request')
            ),
            'parameters' => array(
                'username' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new StringLengthFilter(6, 22), 'USERNAME must have between 6 and 22 characters')
                    )
                ),
                'password' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new NoSpecialCharsRule(), 'PASSWORD field must contain only letters and numbers')
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        // Trying to authenticate
        $dbDriver = DBBrick::getDriver();
        $authentication = new PasswordAuthentication($dbDriver);

        $credentials = $authentication->authenticate($parameters['username'], $parameters['password']);
        if (!is_null($credentials)) {
            $sessionManager = SessionManager::getInstance();
            $sessionManager->startSession(null, true);

            $sessionManager->setSessionVariable('user_id', $credentials->getField(UserCredentialsTable::COLUMN_USER_ID));
            $sessionManager->setSessionVariable('user_permission', $credentials->getField(UserCredentialsTable::COLUMN_PERMISSION));
            $sessionManager->setSessionVariable('client_addr', filter_input(INPUT_SERVER, 'REMOTE_ADDR'));

            $answer->setStatus(1);
            $answer->addMessage('User was successfully authenticated');
            $answer->setContents(array('sess_id' => $sessionManager->getSessionId()));
        } else {
            $answer->setStatus(0);
            $answer->addMessage('Invalid username and/or password');
            $answer->setContents(array());
        }

        return $answer;
    }

}
