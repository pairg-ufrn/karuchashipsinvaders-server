<?php

namespace RoastPHP\Database\Argument\Parser;

class MySQLArgumentArrayParser implements ArgumentParser {

    public function parseToArgument($structure) {
        if ($this->isValidStructure($structure)) {
            $argument = new \RoastPHP\Database\Argument\MySQLArgument;
            $argument->setDbName($structure[ArgumentParser::INDEX_DB_NAME]);
            $argument->setDbPass($structure[ArgumentParser::INDEX_DB_PASS]);
            $argument->setDbUser($structure[ArgumentParser::INDEX_DB_USER]);
            $argument->setHost($structure[ArgumentParser::INDEX_DB_HOST]);
            $argument->setPort($structure[ArgumentParser::INDEX_DB_PORT]);

            return $argument;
        }
        return null;
    }

    private function isValidStructure($structure) {
        if (is_array($structure)) {
            $boolean = (
                    isset($structure[ArgumentParser::INDEX_DB_HOST]) &&
                    isset($structure[ArgumentParser::INDEX_DB_NAME]) &&
                    isset($structure[ArgumentParser::INDEX_DB_PASS]) &&
                    isset($structure[ArgumentParser::INDEX_DB_PORT]) &&
                    isset($structure[ArgumentParser::INDEX_DB_USER]) &&
                    isset($structure[ArgumentParser::INDEX_DB_TYPE])
                    );

            if ($boolean) {
                return $this->verifyType($structure);
            }
        }
        return false;
    }

    private function verifyType($structure) {
        $match = 'mysql';
        $structureType = $structure[ArgumentParser::INDEX_DB_TYPE];
        return (strtolower($structureType) == $match);
    }

}
