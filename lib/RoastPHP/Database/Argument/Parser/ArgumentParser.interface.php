<?php

namespace RoastPHP\Database\Argument\Parser;

interface ArgumentParser{
    
    const INDEX_DB_TYPE = 'db_type';
    const INDEX_DB_HOST = 'db_host';
    const INDEX_DB_USER = 'db_user';
    const INDEX_DB_PASS = 'db_pass';
    const INDEX_DB_NAME = 'db_name';
    const INDEX_DB_PORT = 'db_port';
    
    public function parseToArgument($structure);
    
}