<?php

namespace Brickify\XML;

class XMLSimpleTag extends XMLTag{
    
    public function __construct($tagName, $content = "") {
        parent::__construct($tagName);
        $this->content = $content;
    }

    public function dump() {
        return "<{$this->tagName}>{$this->content}</{$this->tagName}>";
    }

    public function register($element) {
        if(is_string($element)){
            $this->content = $element;
        }
    }

}

