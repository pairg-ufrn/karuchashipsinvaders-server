<?php

namespace Karucha\Rules;

use Brickify\Rules\DataRule;

class IntegerRule implements DataRule{

    public function isValidRule($data = null) {
        return (is_numeric($data));
    }

}