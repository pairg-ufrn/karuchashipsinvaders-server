<?php

namespace Karucha\Tables;

class MatchesTable {

    const TABLE_NAME = 'matches';
    
    const COLUMN_ID = 'id';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_PREVIOUS_MATCH = 'previous_match';
    const COLUMN_STATUS = 'status';
    const COLUMN_CREATED = 'created';
    const COLUMN_MODIFIED = 'modified';

    public static $columns = array(
        self::COLUMN_ID,
        self::COLUMN_USER_ID,
        self::COLUMN_PREVIOUS_MATCH,
        self::COLUMN_CREATED,
        self::COLUMN_MODIFIED
    );

}
