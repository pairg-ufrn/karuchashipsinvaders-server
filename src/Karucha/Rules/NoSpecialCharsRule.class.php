<?php

namespace Karucha\Rules;

use Brickify\Rules\DataRule;

class NoSpecialCharsRule implements DataRule{
    
    public function isValidRule($data = null) {
        return !preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $data);
    }

}
