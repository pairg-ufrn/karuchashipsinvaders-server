<?php

namespace Karucha\Access\Stage;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\UnlockedStageDAO;
use Karucha\Rules\GetRequestRule;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Session\SessionManager;
use Karucha\Tables\UnlockedStagesTable;
use RoastPHP\Util\Sql\SqlCriteria;
use RoastPHP\Util\Sql\SqlExpression;
use RoastPHP\Util\Sql\SqlFilter;

class VerifyUnlockedStageService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new GetRequestRule(), 'VERIFY UNLOCKED STAGE must be triggered by a GET request')
            ),
            'parameters' => array(
                'stage_mode' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STAGE MODE must be an integer value')
                    )
                ),
                'stage_number' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STAGE NUMBER must be an integer value')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        $dbDriver = DBBrick::getDriver();
        $unlockedDAO = new UnlockedStageDAO($dbDriver);
        $sessManager = SessionManager::getInstance();
        $userId = $sessManager->getSessionVariable('user_id');

        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(UnlockedStagesTable::COLUMN_USER_ID, '=', $userId), SqlExpression::AND_OPERATOR);
        $criteria->add(new SqlFilter(UnlockedStagesTable::COLUMN_STAGE_MODE, '=', $parameters['stage_mode']), SqlExpression::AND_OPERATOR);
        $criteria->add(new SqlFilter(UnlockedStagesTable::COLUMN_STAGE_NUMBER, '=', $parameters['stage_number']), SqlExpression::AND_OPERATOR);

        $selected = $unlockedDAO->get($criteria);
        if (empty($selected)) {
            $answer->setMessages(array('Stage is locked'));
            $answer->setContents(array('is_stage_unlocked' => 0));
        } else {
            $answer->setMessages(array('Stage is unlocked'));
            $answer->setContents(array('is_stage_unlocked' => 1));
        }

        return $answer;
    }

}
