<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\StagesTable;
use PDO;

class StageDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(StagesTable::TABLE_NAME, $driver);
    }
    
}