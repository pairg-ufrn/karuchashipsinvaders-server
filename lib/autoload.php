<?php
while (!file_exists(getcwd() . "/.htaccess")) {
    chdir('..');
}

require_once 'conf/AppConfig.inc.php';
require_once 'conf/AppDatabase.inc.php';
require_once 'conf/AppServices.inc.php';

function brickify_autoload($className) {

    $packages = array(
        'src',
        'vendors',
        'lib'
    );

    $extensions = array(
        '.abstract.php',
        '.class.php',
        '.interface.php',
        '.php'
    );

    $filePath = str_replace('\\', DIRECTORY_SEPARATOR, $className);

    foreach ($extensions as $extension) {
        foreach ($packages as $package) {
            $packageDir = (($package == '\\' || $package == '/') ? '' : $package . DIRECTORY_SEPARATOR);
            $includePath = getcwd() . DIRECTORY_SEPARATOR . $packageDir . $filePath . $extension;
            if (is_file($includePath) && is_readable($includePath)) {
                include_once $includePath;
                break;
            }
        }
    }
}

spl_autoload_register("brickify_autoload");

