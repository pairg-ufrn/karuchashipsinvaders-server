<?php

use Brickify\Database\DBBrick;

require_once 'lib/autoload.php';

$connected = !is_null(DBBrick::getDriver());
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Karucha Ships Invaders - Server</title>

        <!-- Bootstrap core CSS -->
        <link href="res/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="res/css/style.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">
            <center>
                <a href="https://code.google.com/p/karuchashipsinvaders/" target="_blank"><img src="res/img/icone.png" width="296" height="296"/></a>
                <br/>
                <?php 
                if($connected){
                    ?>
                        <h4>Server Status: <strong class="green-text">Installed</strong></h4>
                    <?php
                }else{
                    ?>
                        <h4>Server Status: <a href="install/"><strong class="red-text">Not installed (click here to install)</strong></a></h4>
                    <?php
                }
                
                ?>
            </center>
            <a href="https://code.google.com/p/karuchashipsinvaders/wiki/equipe" class="about" target="_blank">
                
            </a>
        </div> <!-- /container -->

    </body>
</html>
