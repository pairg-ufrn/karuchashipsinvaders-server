<?php

namespace Brickify\XML;

class XMLCompositeTag extends XMLTag {

    public function __construct($tagName) {
        parent::__construct($tagName);
        $this->content = array();
    }

    public function dump() {
        // Open tag
        $xml = "<" . $this->tagName . ">";
        $content = "";
        foreach ($this->content as $tag) {
            $content .= $tag->dump();
        }
        $xml .= $content;

        // Close tag
        $xml .= "</" . $this->tagName . ">";

        return $xml;
    }

    public function register($element) {
        if (is_object($element)) {
            if ($element instanceof XMLTag) {
                $this->content[] = $element;
            }
        }
    }

}
