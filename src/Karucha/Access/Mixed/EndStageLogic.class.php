<?php

namespace Karucha\Access\Mixed;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\Access\Glyphs\AddGlyphService;
use Karucha\Access\Stage\CloseStageService;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Rules\Stage\StageExistsRule;
use Karucha\Rules\ValueInSetRule;

class EndStageLogic extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'parameters' => array(
                'stage_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STAGE ID must be an integer value'),
                        new ServiceFilter(new StageExistsRule(), 'The given STAGE ID does not exists')
                    )
                ),
                'play_time' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'PLAY TIME must be an integer value'),
                    )
                ),
                'status' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STATUS must be an integer value'),
                        new ServiceFilter(new ValueInSetRule(array(0, 1)), 'Invalid STATUS')
                    )
                ),
                'glyphs' => array(
                    'required' => true,
                ),
                'help_calls' => array(
                    'required' => false,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'HELP CALLS must be an integer value')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $stageId = $parameters['stage_id'];
        $success = true;
        $stageClosed = true;
        // Saving all the glyphs
        $addGlyphService = new AddGlyphService();

        foreach ($parameters['glyphs'] as $glyph) {
            $glyph['stage_id'] = $stageId;
            $glyphAnswer = $addGlyphService->call($glyph);
            if ($glyphAnswer->getStatus() == 0) {
                $success = false;
//                foreach ($glyphAnswer->getMessages() as $glyphMessage) {
//                    $answer->setStatus(0);
//                    $answer->addMessage($glyphMessage);
//                    $answer->setContents(array());
//                }
                break;
            }
        }
        
        if ($success) {
            $closeStageService = new CloseStageService();
            $closeStageParameters = array(
                'stage_id' => $stageId,
                'play_time' => $parameters['play_time'],
                'status' => $parameters['status'],
                'sess_id' => $parameters['sess_id']
            );

            if(isset($parameters['help_calls'])){
                $closeStageParameters['help_calls'] = $parameters['help_calls'];
            }
            
            $stageAnswer = $closeStageService->call($closeStageParameters);
            if ($stageAnswer->getStatus() == 0) {
                $stageClosed = false;
//                foreach ($stageAnswer->getMessages() as $stageMessage) {
//                    $answer->setStatus(0);
//                    $answer->addMessage($stageMessage);
//                    $answer->setContents(array());
//                }
            }
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Error while saving the glyphs. Aborting stage close'));
            $answer->setContents(array());
        }

        if ($stageClosed) {
            $answer->setContents(array());
            $answer->setMessages(array('Stage closed successfully. All glyphs were saved'));
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Error while closing stage'));
            $answer->setContents(array());
        }

        return $answer;
    }

}
