<?php

namespace Brickify\ResultSet;

class ResultSetSlot {

    private $resultSet;

    public function __construct($resultSet) {
        if (is_array($resultSet)) {
            $this->resultSet = $resultSet;
        }
    }

    public function isValid() {
        return !is_null($this->resultSet);
    }

    public function getField($columnName) {
        return isset($this->resultSet[$columnName]) ? $this->resultSet[$columnName] : null;
    }

    public function setField($columnName, $value) {
        $this->resultSet[$columnName] = $value;
        return true;
    }

    public function belongsToTable($tableColumns) {
        $belongs = true;
        foreach ($tableColumns as $tableColumn) {
            $belongs = array_key_exists($tableColumn, $this->resultSet);
            if (!$belongs) {
                break;
            }
        }
        return $belongs;
    }

    public function getResultSet() {
        return $this->resultSet;
    }

}
