<?php

namespace RoastPHP\Util\Sql;

class InnerJoin extends Join{
	
	public function dump(){
		return "INNER JOIN {$this->entity} ON ".$this->criteria->dump();
	}
	
}