<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\MatchesTable;
use PDO;

class MatchDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(MatchesTable::TABLE_NAME, $driver);
    }

}