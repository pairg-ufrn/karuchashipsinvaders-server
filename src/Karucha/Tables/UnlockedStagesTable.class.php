<?php

namespace Karucha\Tables;

class UnlockedStagesTable{
    
    const TABLE_NAME = 'unlocked_stages';
    
    const COLUMN_ID = 'id';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_STAGE_NUMBER = 'stage_number';
    const COLUMN_STAGE_MODE = 'stage_mode';
    const COLUMN_CREATED = 'created';
    
    public static $column = array(
        self::COLUMN_ID,
        self::COLUMN_USER_ID,
        self::COLUMN_STAGE_NUMBER,
        self::COLUMN_STAGE_MODE,
        self::COLUMN_CREATED
    );
    
}