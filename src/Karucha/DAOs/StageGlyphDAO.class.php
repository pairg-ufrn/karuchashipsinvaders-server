<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\StageGlyphsTable;
use PDO;

class StageGlyphDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(StageGlyphsTable::TABLE_NAME, $driver, false, false);
    }
    
}

