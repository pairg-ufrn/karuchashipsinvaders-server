<?php

namespace Brickify\Operation;

use Brickify\Answer\BrickifyAnswer;

class LinkedOperation {

    private $primaryAction;

    public function runOperation($starterParameters = null) {
        $previousAction = null;
        $currentAction = $this->primaryAction;

        $answer = new BrickifyAnswer();
        $answer->setStatus(1);

        while (!is_null($currentAction) && ($answer->getStatus() == 1)) {
            var_dump($currentAction);
            if ($currentAction->requiresParameters()) {
                if (is_null($previousAction)) {
                    $runParameters = $starterParameters;
                } else {
                    $runParameters = $previousAction->getGeneratedContents();
                }
            } else {
                $runParameters = null;
            }

            $currentAction->runAction($runParameters);
            if ($currentAction->getActionStatus() <= 0) {
                $answer->setStatus(0);
                $answer->addMessage($currentAction->getErrorMessage());
            }
            
            
        }

        return $answer;
    }

    public function addAction(LinkedAction $action) {
        $parent = $this->primaryAction;
        $focused = $this->primaryAction;

        if (is_null($parent)) {
            $this->primaryAction = $action;
        } else {
            while (!is_null($focused)) {
                $temp = $focused;
                $focused = $focused->getChild();
                $parent = $temp;
            }

            $parent->setChild($action);
        }
    }

}
