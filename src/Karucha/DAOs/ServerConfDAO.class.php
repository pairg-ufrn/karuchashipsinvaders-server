<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\ServerConfTable;
use PDO;

class ServerConfDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(ServerConfTable::TABLE_NAME, $driver, true, false);
    }
    
}