<?php

namespace Karucha\Access\Stage;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\StageDAO;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Rules\Stage\StageExistsRule;
use Karucha\Tables\StagesTable;

class CloseStageService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'CLOSE STAGE operation must be triggered by a post request')
            ),
            'parameters' => array(
                'stage_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STAGE ID must be an integer value'),
                        new ServiceFilter(new StageExistsRule(), 'The given STAGE does not exists')
                    )
                ),
                'play_time' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'PLAY TIME must be an integer value')
                    )
                ),
                'status' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STATUS must be an integer value')
                    )
                ),
                'help_calls' => array(
                    'required' => false,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'HELP CALLS must be an integer value')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $driver = DBBrick::getDriver();
        $dao = new StageDAO($driver);

        $resultSet = $dao->get($parameters['stage_id']);

        $resultSet->setField(StagesTable::COLUMN_STATUS, $parameters['status']);
        $resultSet->setField(StagesTable::COLUMN_PLAY_TIME, $parameters['play_time']);
        $resultSet->setField(StagesTable::COLUMN_CONTEXT_HELP_CALLS, $parameters['help_calls']);


        $affected = $dao->save($resultSet);
        if ($affected == 1) {
            $answer->setStatus(1);
            $answer->setMessages(array('Stage successfully saved'));
            $answer->setContents(array('stage_id' => $parameters['stage_id']));
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Error while trying to save the stage. Try again later'));
            $answer->setContents(array());
        }

        return $answer;
    }

}
