<?php

namespace Karucha\Rules;

use Brickify\Rules\DataRule;

class PostRequestRule implements DataRule{
    
    public function isValidRule($data = null) {
        return (empty($_GET) && !empty($_POST));
    }

}