<?php

namespace Karucha\Tables;

class StagesTable{
    
    const TABLE_NAME = 'stages';
    
    const COLUMN_ID = 'id';
    const COLUMN_MATCH_ID = 'match_id';
    const COLUMN_LEVEL_NUMBER = 'level_number';
    const COLUMN_LEVEL_MODE = 'level_mode';
    const COLUMN_STATUS = 'status';
    const COLUMN_PLAY_TIME = 'play_time';
    const COLUMN_CONTEXT_HELP_CALLS = 'context_help_calls';
    const COLUMN_CREATED = 'created';
    const COLUMN_MODIFIED = 'modified';
    
    public static $columns = array(
        self::COLUMN_ID,
        self::COLUMN_MATCH_ID,
        self::COLUMN_LEVEL_NUMBER,
        self::COLUMN_LEVEL_MODE,
        self::COLUMN_STATUS,
        self::COLUMN_PLAY_TIME,
        SELF::COLUMN_CONTEXT_HELP_CALLS,
        self::COLUMN_CREATED,
        self::COLUMN_MODIFIED
    );
}