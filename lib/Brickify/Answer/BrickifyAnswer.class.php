<?php

namespace Brickify\Answer;

class BrickifyAnswer implements ServiceAnswer{

    private $status;
    private $messages;
    private $contents;

    public function __construct(){
        $this->status = -1;
        $this->messages = array();
        $this->contents = array();
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function getMessages() {
        return $this->messages;
    }

    public function getContent() {
        return $this->contents;
    }

    public function setStatus($x) {
        $this->status = $x;
    }

    public function setMessages($x) {
        if(is_array($x)){
            $this->messages = $x;
        }
    }
    
    public function addMessage($x){
        if(!$this->messages){
            $this->messages = array();
        }
        
        if(is_string($x)){
            $this->messages[] = $x;
        }
    }

    public function setContents($x) {
        if (is_array($x)) {
            $this->contents = $x;
        }
    }

    public function addContent($x){
        if(!$this->contents){
            $this->contents = array();
        }
        
        $this->contents[] = $x;
    }
    
    public function toArray(){
        return array('status' => $this->status, 'messages' => $this->messages, 'contents' => $this->contents);
    }

    public function pack() {
        echo json_encode($this->toArray());
    }

}