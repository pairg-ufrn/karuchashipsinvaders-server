<?php

namespace Karucha\Access\Stage;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\ResultSet\ResultSetSlot;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\StageDAO;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\Match\MatchExistsRule;
use Karucha\Rules\Match\MatchIsOpenRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Tables\StagesTable;

class OpenStageService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'OPEN STAGE must be triggered by a post request')
            ),
            'parameters' => array(
                'match_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'MATCH ID must be an integer value'),
                        new ServiceFilter(new MatchExistsRule(), 'The given match does not exists'),
                        new ServiceFilter(new MatchIsOpenRule, 'The given match is not open')
                    )
                ),
                'level_number' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'LEVEL NUMBER must be an integer value')
                    )
                ),
                'level_mode' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'LEVEL MODE must be an integer value')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $stageStructure = array(
            StagesTable::COLUMN_MATCH_ID => $parameters['match_id'],
            StagesTable::COLUMN_LEVEL_NUMBER => $parameters['level_number'],
            StagesTable::COLUMN_LEVEL_MODE => $parameters['level_mode'],
            StagesTable::COLUMN_STATUS => 0
        );

        $resultSet = new ResultSetSlot($stageStructure);

        $driver = DBBrick::getDriver();
        $dao = new StageDAO($driver);

        $affected = $dao->save($resultSet);
        if ($affected == 1) {
            $answer->setStatus(1);
            $answer->setMessages(array('Stage successfully opened'));
            $answer->setContents(array('stage_id' => $resultSet->getField(StagesTable::COLUMN_ID)));
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Error while saving your stage. Try again later'));
            $answer->setContents(array());
        }

        return $answer;
    }

}
