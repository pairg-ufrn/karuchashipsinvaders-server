<?php

namespace RoastPHP\Database;

use PDO;
use PDOException;
use RoastPHP\Database\Argument\DbArgument;

/**
 * Static class, used to connect to the database
 * 
 * Por meio desta classe, obtemos uma conexão com o banco de dados
 * baseado no objeto PDO padrão do PHP.
 * 
 * @author Alison Bento <alisonlks@outlook.com>
 * @version 1.0.0
 * @static
 */
final class DbDriver {

    /**
     * Open the database connection
     * 
     * @static
     * @param DbArgument $argument The argument to open the connection
     */
    public static function open(DbArgument $argument) {
        try{
            $pdo = new PDO($argument->getDSN(), $argument->getLogin(), $argument->getPassword());
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $ex) {
            return null;
        }
    }

}
