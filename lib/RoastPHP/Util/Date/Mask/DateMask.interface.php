<?php

namespace RoastPHP\Util\Date\Mask;

interface DateMask{
	
	public function getMask();
	
}