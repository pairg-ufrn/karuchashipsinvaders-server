<?php

namespace Karucha\Rules\Match;

use Brickify\Database\DBBrick;
use Brickify\Rules\DataRule;
use Karucha\DAOs\MatchDAO;

class MatchExistsRule implements DataRule{
    
    
    public function isValidRule($data = null) {
        
        $driver = DBBrick::getDriver();
        $dao = new MatchDAO($driver);
        
        return (!is_null($dao->get($data)));
    }

}