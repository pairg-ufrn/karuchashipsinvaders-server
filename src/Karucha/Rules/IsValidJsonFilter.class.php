<?php

namespace Karucha\Rules;

use Brickify\Rules\DataRule;

class IsValidJsonFilter implements DataRule{
    
    public function isValidRule($data = null) {
        return !is_null(json_decode($data, true));
    }

}