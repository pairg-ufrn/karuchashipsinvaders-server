<?php

use Brickify\Services\BrickifyEmptyService;
use Karucha\Access\Glyphs\AddGlyphService;
use Karucha\Access\Match\CloseMatchService;
use Karucha\Access\Match\OpenMatchService;
use Karucha\Access\Mixed\EndStageService;
use Karucha\Access\Server\ServerVersionService;
use Karucha\Access\Session\AddUserService;
use Karucha\Access\Session\LoginService;
use Karucha\Access\Session\LogoutService;
use Karucha\Access\Stage\AllUnlockedStagesService;
use Karucha\Access\Stage\CloseStageService;
use Karucha\Access\Stage\OpenStageService;
use Karucha\Access\Stage\UnlockStageService;
use Karucha\Access\Stage\VerifyUnlockedStageService;
use Karucha\Access\Support\IsSupportedVersionService;

function brickify_app_services(){
    
    return array(
        // Empty service
        'empty' => new BrickifyEmptyService(),
        
        // Match Services
        'open_match' => new OpenMatchService(),
        'close_match' => new CloseMatchService(),
        
        // Stage Services
        'open_stage' => new OpenStageService(),
        'close_stage' => new CloseStageService(),
        'end_stage' => new EndStageService(),
        'unlock_stage' => new UnlockStageService(),
        'verify_unlocked_stage' => new VerifyUnlockedStageService(),
        'all_unlocked_stages' => new AllUnlockedStagesService(),
        
        // Glyph Services
        'add_glyph' => new AddGlyphService(),
        
        // Session
        'login' => new LoginService(),
        'logout' => new LogoutService(),
        
        // User
        'add_user' => new AddUserService(),
        
        // Version
        'version' => new ServerVersionService(),
        
        'verify_compatibility' => new IsSupportedVersionService()
    );
}

