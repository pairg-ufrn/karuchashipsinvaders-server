<?php

namespace Brickify\Database;

use RoastPHP\Database\Argument\MySQLArgument;
use RoastPHP\Database\DbDriver;

class DBBrick {

    private static $connection = null;

    public static function getDriver() {
        if (is_null(self::$connection)) {
            $dbData = brickify_database_data();
            if(empty($dbData)){
                $dbData = BRICKIFY_DB_FILE;
            }
            $databaseFile = new DBFile($dbData);
            
            if ($databaseFile->isGeneric()) {
                return null;
            }
            
            $argument = null;
            switch ($databaseFile->getDbType()) {
                case 'mysql':
                    $argument = new MySQLArgument();
                    break;
            }

            $argument->setHost($databaseFile->getDbHost());
            $argument->setPort($databaseFile->getDbPort());
            $argument->setDbUser($databaseFile->getDbUser());
            $argument->setDbPass($databaseFile->getDbPass());
            $argument->setDbName($databaseFile->getDbName());

            self::$connection = DbDriver::open($argument);
        }

        return self::$connection;
    }

}
