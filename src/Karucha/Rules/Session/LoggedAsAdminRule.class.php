<?php

namespace Karucha\Rules\Session;

use Brickify\Rules\DataRule;
use Karucha\Session\SessionManager;

class LoggedAsAdminRule implements DataRule{
    
    public function isValidRule($data = null) {
        $sessionManager = SessionManager::getInstance();
        $sessionManager->startSession($data);
        $permission = $sessionManager->getSessionVariable('user_permission');
        return (!is_null($permission) && $permission == 0);
    }

}