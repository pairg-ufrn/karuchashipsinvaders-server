<?php

namespace RoastPHP\Util\Encrypt;

class Md5Hash implements EncryptedData {

    public function encrypt($data) {
        return md5($data);
    }

}
