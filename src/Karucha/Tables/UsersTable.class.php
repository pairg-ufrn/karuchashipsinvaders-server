<?php

namespace Karucha\Tables;

class UsersTable{
    
    const TABLE_NAME = 'users';
    
    const COLUMN_ID = 'id';
    const COLUMN_NAME = 'name';
    const COLUMN_NICKNAME = 'nickname';
    const COLUMN_EMAIL = 'email';
    const COLUMN_CREATED = 'created';
    const COLUMN_MODIFIED = 'modified';
    
    public static $columns = array(
        self::COLUMN_ID,
        self::COLUMN_NAME,
        self::COLUMN_NICKNAME,
        self::COLUMN_EMAIL,
        self::COLUMN_CREATED,
        self::COLUMN_MODIFIED
    );
    
}