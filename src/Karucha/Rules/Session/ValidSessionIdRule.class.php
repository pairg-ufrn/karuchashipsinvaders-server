<?php

namespace Karucha\Rules\Session;

use Brickify\Rules\DataRule;
use Karucha\Session\SessionManager;

class ValidSessionIdRule implements DataRule{
    
    public function isValidRule($data = null) {
        $sessionManager = SessionManager::getInstance();
        $sessionManager->startSession($data);
        return !is_null($sessionManager->getSessionVariable('user_id'));
    }

}