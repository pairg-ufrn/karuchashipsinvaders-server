<?php

namespace Brickify\ConfigFiles;

use Exception;

class BrickifySafeFile {

    const DEFAULT_CONF_DIR = '/conf/files/';
    const DEFAULT_CONF_EXT = 'bfsi';

    private $configFilename;
    private $content;

    public function __construct($configFilename) {
        $this->configFilename = $configFilename;
        if (is_file($this->getFullFilename())) {
            $fileContents = file_get_contents($this->getFullFilename());
            $jsonContent = json_decode($fileContents, true);
            if(!is_null($jsonContent)){
                $this->content = $jsonContent;
            }else{
                $this->content = array();
            }
            
        } else {
            $this->content = array();
        }
    }

    public function getFullFilename() {
        return getcwd() . self::DEFAULT_CONF_DIR . $this->configFilename . '.' . self::DEFAULT_CONF_EXT;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        if (is_array($content)) {
            $this->content = $content;
        }
    }

    public function writeToFile() {
        if (!file_exists($this->getFullFilename()) || is_writable($this->getFullFilename())) {
            $jsonData = json_encode($this->content);
            if (!is_null($jsonData)) {
                $saved = file_put_contents($this->getFullFilename(), $jsonData);
            } else {
                throw new Exception('Invalid file content to "' . $this->configFilename . '"');
            }
        } else {
            throw new Exception('Cannot write to file "' . $this->configFilename . '": forbidden');
        }
        
        return $saved;
    }

    public function isWritable(){
        return is_writable($this->getFullFilename());
    }
    
}
