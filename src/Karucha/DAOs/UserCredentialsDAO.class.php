<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\UserCredentialsTable;
use PDO;

class UserCredentialsDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(UserCredentialsTable::TABLE_NAME, $driver);
    }
    
}
