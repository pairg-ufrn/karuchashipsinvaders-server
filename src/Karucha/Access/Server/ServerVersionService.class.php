<?php

namespace Karucha\Access\Server;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Services\BrickifyService;

class ServerVersionService extends BrickifyService{
    
    protected function serviceConditions() {
        return array();
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        
        $answer->setStatus(1);
        $answer->setMessages(array('Karucha server version ' . SERVER_VERSION));
        $answer->setContents(array('server_version' => SERVER_VERSION));
        
        return $answer;
    }

}