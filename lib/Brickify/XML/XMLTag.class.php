<?php

namespace Brickify\XML;

abstract class XMLTag{
    
    protected $tagName;
    protected $content;
    
    public function __construct($tagName){
        $this->tagName = $tagName;
        $this->content = null;
    }
    
    abstract public function register($element);
    
    public function isSimpleTag(){
        return is_string($this->content);
    }
    
    abstract public function dump();
}