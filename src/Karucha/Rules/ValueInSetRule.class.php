<?php

namespace Karucha\Rules;

use Brickify\Rules\DataRule;

class ValueInSetRule implements DataRule {

    private $set;

    public function __construct($set) {
        if (is_array($set)) {
            $this->set = $set;
        }else{
            $this->set = array();
        }
    }

    public function isValidRule($data = null) {
        return in_array($data, $this->set);
    }

}
