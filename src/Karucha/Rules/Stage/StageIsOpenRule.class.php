<?php

namespace Karucha\Rules\Stage;

use Brickify\Database\DBBrick;
use Brickify\Rules\DataRule;
use Karucha\DAOs\StageDAO;
use Karucha\Tables\StagesTable;

class StageIsOpenRule implements DataRule{
    
    public function isValidRule($data = null) {
        
        $driver = DBBrick::getDriver();
        $dao = new StageDAO($driver);
        
        $resultSet = $dao->get($data);
        return ($resultSet->getField(StagesTable::COLUMN_STATUS) == 0);
        
    }

}