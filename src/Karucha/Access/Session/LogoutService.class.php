<?php

namespace Karucha\Access\Session;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Session\SessionManager;

class LogoutService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'LOGOUT must be triggered by a HTTP POST request')
            ),
            'parameters' => array(
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Session was not opened with the given parameters'),
                        new ServiceFilter(new SameClientRule(), 'Session could not be opened')
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        $sessionManager = SessionManager::getInstance();
        $sessionManager->destroySession($parameters['sess_id']);

        $answer->setStatus(1);
        $answer->addMessage("The user has logged out successfully");
        $answer->setContents(array());

        return $answer;
    }

}
