<?php

namespace Karucha\Access\Glyphs;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\ResultSet\ResultSetSlot;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\StageGlyphDAO;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Stage\StageExistsRule;
use Karucha\Rules\Stage\StageIsOpenRule;
use Karucha\Tables\StageGlyphsTable;

class AddGlyphService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
            new ServiceFilter(new PostRequestRule(), 'ADD GLYPH must be triggered by a post request')
            ),
            'parameters' => array(
                'stage_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new StageExistsRule(), 'The given stage does not exists'),
                        new ServiceFilter(new StageIsOpenRule(), 'The given stage is not opened')
                    )
                ),
                'glyph_type' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'GLYPH TYPE must be an integer value')
                    )
                ),
                'glyph_column' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'GLYPH COLUMN must be an integer value')
                    )
                ),
                'glyph_row' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'GLYPH ROW must be an integer value')
                    )
                ),
                'status' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STATUS must be an integer value')
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();

        $glyphStructure = array(
            StageGlyphsTable::COLUMN_STAGE_ID => $parameters['stage_id'],
            StageGlyphsTable::COLUMN_TYPE => $parameters['glyph_type'],
            StageGlyphsTable::COLUMN_ROW => $parameters['glyph_row'],
            StageGlyphsTable::COLUMN_COLUMN => $parameters['glyph_column'],
            StageGlyphsTable::COLUMN_STATUS => $parameters['status'],
        );

        $resultSet = new ResultSetSlot($glyphStructure);

        $driver = DBBrick::getDriver();
        $dao = new StageGlyphDAO($driver);

        $affected = $dao->save($resultSet);
        if ($affected == 1) {
            $answer->setStatus(1);
            $answer->setMessages(array('Glyph successfully saved'));
            $answer->setContents(array());
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Error while trying to save the glyph'));
            $answer->setContents(array());
        }
        
        return $answer;
    }

}
