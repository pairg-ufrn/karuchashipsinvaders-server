<?php

namespace Karucha\Tables;

class UserCredentialsTable{
    
    const TABLE_NAME = 'user_credentials';
    
    const COLUMN_ID = 'id';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_USERNAME = 'username';
    const COLUMN_PASSWORD = 'password';
    const COLUMN_ITERATIONS = 'iterations';
    const COLUMN_PERMISSION = 'permission';
    const COLUMN_CREATED = 'created';
    const COLUMN_MODIFIED = 'modified';
    
    public static $columns = array(
        self::COLUMN_ID,
        self::COLUMN_USER_ID,
        self::COLUMN_USERNAME,
        self::COLUMN_PASSWORD,
        self::COLUMN_ITERATIONS,
        self::COLUMN_CREATED,
        self::COLUMN_MODIFIED
    );
    
}