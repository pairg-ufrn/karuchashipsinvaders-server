<?php

namespace Karucha\Session;

use RoastPHP\Util\Encrypt\Base64Reverse;
use RoastPHP\Util\Encrypt\Sha1Hash;

final class SessionManager {

    const RECURSION = 7;
    
    private static $instance;
    
    private function __construct() {
        
    }

    /**
     * @return SessionManager
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new SessionManager();
        }

        return self::$instance;
    }

    public function startSession($sessionId = null, $forceNewId = false) {
        if (!$this->isSessionStarted()) {
            if(!is_null($sessionId)){
                session_id($sessionId);
            }
            session_start();
            if($forceNewId){
                session_regenerate_id();
            }
        }
    }

    public function destroySession($sessionId = null) {
        if (!$this->isSessionStarted()){
            $this->startSession($sessionId);
        }
        session_destroy();
        session_unset();
    }

    public function isSessionStarted() {
        return (session_id() != '');
    }

    public function getSessionId(){
        return (session_id());
    }
    
    public function getSessionVariable($varName) {
        if (session_id() == '') {
            $this->startSession();
        }

        $returnValue = null;

        $hash = new Sha1Hash();
        $index = $hash->encrypt($varName);
        $infocrypter = new Base64Reverse(self::RECURSION);
        
        if (isset($_SESSION[$index])) {
            $returnValue = $infocrypter->decrypt($_SESSION[$index]);
        }

        return $returnValue;
    }

    public function setSessionVariable($varName, $varValue) {
        if (session_id() == ''){
            $this->startSession();
        }
            
        $hash = new Sha1Hash();
        $infocrypter = new Base64Reverse(self::RECURSION);
        $index = $hash->encrypt($varName);

        $_SESSION[$index] = $infocrypter->encrypt($varValue);
    }
}
