<?php

namespace RoastPHP\Util\Date\Mask;

class YMDFormat implements DateMask{
	
	private $date;
	
	public function __construct(\RoastPHP\Util\Date\Date $date){
		$this->date = $date;
	}
	
	public function getMask(){
		$date = $this->date;
		return "{$date->getYear()}/{$date->getMouth()}/{$date->getDay()} {$date->getHours()}:{$date->getMinutes()}:{$date->getSeconds()}";
	}
	
}