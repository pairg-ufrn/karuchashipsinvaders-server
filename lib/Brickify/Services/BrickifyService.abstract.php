<?php

namespace Brickify\Services;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Answer\ServiceAnswer;

abstract class BrickifyService {

    abstract protected function serviceConditions();

    abstract protected function onSuccess($parameters);

    public function call($parameters = null) {

        $answer = new BrickifyAnswer();
        
        $answer->setStatus(1);
        $this->filter($parameters, $answer);

        if ($answer->getStatus() == 1) {
            $successAnswer = $this->onSuccess($parameters);
            if ($successAnswer instanceof ServiceAnswer) {
                $answer = $successAnswer;
            } else {
                $answer->setStatus(0);
                $answer->setMessages(array('Invalid implemented "onSuccess" method. Must return a ServiceAnswer'));
                $answer->setContents(array());
            }
        }

        return $answer;
    }

    public function dumpParameters() {
        $parameters = $this->serviceConditions();
        if (isset($parameters['parameters'])) {
            if (!empty($parameters['parameters'])) {
                return array_keys($parameters['parameters']);
            }
        }
        return array();
    }

    private function filter($parameters, &$answer) {
        // Indica se devemos parar devido a erros
        $shouldBreak = false;

        $serviceConditions = $this->serviceConditions();
        // Se existem filtros para a operação que estamos fazendo
        if (isset($serviceConditions['operation'])) {

            // Para cada filtro cadastrado
            foreach ($serviceConditions['operation'] as $operationFilter) {

                // Filtrando a operação e verificando se obtivemos sucesso
                $shouldBreak = $this->filterOperation($operationFilter, $answer);

                // Verificando se devemos parar
                if ($shouldBreak) {
                    break;
                }
            }
        }

        // Se existem filtros para parâmetros e deu tudo certo com os filtros de operação
        if (isset($serviceConditions['parameters']) && !$shouldBreak) {

            // Obtendo a lista de parametros e seus filtros
            foreach ($serviceConditions['parameters'] as $serviceName => $serviceParameter) {

                // Se o parâmetro em questão é necessário para a operação
                $required = isset($serviceParameter['required']) ? $serviceParameter['required'] : false;

                // Se existem filtros aplicados a esse parâmetro
                $filters = isset($serviceParameter['filters']) ? $serviceParameter['filters'] : array();

                // Realizamos a filtragem e resgatamos o resultado
                $shouldBreak = $this->filterInput($serviceName, $required, $filters, $parameters, $answer);

                // Verificando se devemos parar
                if ($shouldBreak) {
                    break;
                }
            }
        }

        // Não há necessidade de retorno uma vez que todas as informações ficam salvas na entidade Answer
    }

    private function filterOperation(ServiceFilter $filter, BrickifyAnswer &$answer) {
        $shouldBreak = false;

        $dataRule = $filter->getRule();
        if (!$dataRule->isValidRule()) {
            $answer->setStatus(0);
            $answer->addMessage($filter->getErrorMessage());
            if ($filter->isCritical()) {
                $shouldBreak = true;
            }
        }

        return $shouldBreak;
    }

    private function filterInput($parameterName, $required, $filters, $parameters, BrickifyAnswer &$answer) {
        $shouldBreak = false;
        if (isset($parameters[$parameterName])) {
            $value = $parameters[$parameterName];
            foreach ($filters as $parameterFilter) {
                $shouldBreak = $this->filterValue($parameterFilter, $value, $answer);
                if ($shouldBreak) {
                    break;
                }
            }
        } else if ($required) {
            $answer->setStatus(0);
            $answer->addMessage('The field ' . strtoupper($parameterName) . ' is required');
        }

        return $shouldBreak;
    }

    /**
     * Filters a given data
     * @param ServiceFilter $filter
     * @param type $value
     * @param BrickifyAnswer $answer
     * @return boolean
     */
    private function filterValue(ServiceFilter $filter, $value, BrickifyAnswer &$answer) {
        $shouldBreak = false;
        $dataRule = $filter->getRule();
        if (!$dataRule->isValidRule($value)) {
            $answer->setStatus(0);
            $answer->addMessage($filter->getErrorMessage());
            if ($filter->isCritical()) {
                $shouldBreak = true;
            }
        }

        return $shouldBreak;
    }

}
