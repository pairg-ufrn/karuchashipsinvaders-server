<?php

namespace Karucha\Access\Support;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Services\BrickifyService;

require_once 'conf/AppCompatibility.inc.php';

class IsSupportedVersionService extends BrickifyService {

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $allSupported = karucha_clients_supported();

        if (in_array($parameters['client_version'], $allSupported)) {
            $answer->setStatus(1);
            $answer->setMessages(array('This version is supported by the server'));
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('This version is not supported by the server'));
        }

        return $answer;
    }

    protected function serviceConditions() {
        return array(
            'parameters' => array(
                'client_version' => array(
                    'required' => true
                )
            )
        );
    }

}
