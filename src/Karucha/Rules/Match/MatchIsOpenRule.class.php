<?php

namespace Karucha\Rules\Match;

use Brickify\Database\DBBrick;
use Brickify\Rules\DataRule;
use Karucha\DAOs\MatchDAO;
use Karucha\Tables\MatchesTable;

class MatchIsOpenRule implements DataRule{
    
    
    public function isValidRule($data = null) {
        $driver = DBBrick::getDriver();
        $dao = new MatchDAO($driver);
        
        $resultSet = $dao->get($data);
        return ($resultSet->getField(MatchesTable::COLUMN_STATUS) == 0);
    }

}

