<?php

namespace Karucha\DAOs;

use Brickify\DAOs\BrickifyDAO;
use Karucha\Tables\UsersTable;
use PDO;

class UserDAO extends BrickifyDAO{
    
    public function __construct(PDO $driver) {
        parent::__construct(UsersTable::TABLE_NAME, $driver);
    }
    
}

