<?php

namespace Karucha\Access\Stage;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\ResultSet\ResultSetSlot;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\UnlockedStageDAO;
use Karucha\Rules\IntegerRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;
use Karucha\Session\SessionManager;
use Karucha\Tables\UnlockedStagesTable;

class UnlockStageService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'UNLOCK STAGE must be triggered by a post request')
            ),
            'parameters' => array(
                'stage_mode' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STAGE MODE must be an integer value')
                    )
                ),
                'stage_number' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IntegerRule(), 'STAGE NUMBER must be an integer value')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $dbDriver = DBBrick::getDriver();
        $unlockDAO = new UnlockedStageDAO($dbDriver);
        $sessManager = SessionManager::getInstance();
        $userId = $sessManager->getSessionVariable('user_id');

        $unlockStructure = array(
            UnlockedStagesTable::COLUMN_USER_ID => $userId,
            UnlockedStagesTable::COLUMN_STAGE_MODE => $parameters['stage_mode'],
            UnlockedStagesTable::COLUMN_STAGE_NUMBER => $parameters['stage_number']
        );

        $rsUnlock = new ResultSetSlot($unlockStructure);
        $affected = $unlockDAO->save($rsUnlock);
        if ($affected == 1) {
            $answer->setStatus(1);
            $answer->setMessages(array('Stage unlocked successfully'));
            $answer->setContents(array());
        } else {
            $answer->setStatus(0);
            $answer->setMessages(array('Stage could not be unlocked. Try again'));
            $answer->setContents(array());
        }

        return $answer;
    }

}
