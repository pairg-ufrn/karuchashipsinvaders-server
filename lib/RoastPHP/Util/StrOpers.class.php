<?php

namespace RoastPHP\Util;

final class StrOpers {

    // Code available at http://www.paulferrett.com/2009/php-camel-case-functions/
    // Access 2013-11-12 02:44 (Brazil)
    public static function fromCamelCase($str) {
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "_" . strtolower($c[1]);');
        return preg_replace_callback('/([A-Z])/', $func, $str);
    }

    // Code available at http://www.paulferrett.com/2009/php-camel-case-functions/
    // Access 2013-11-12 02:44 (Brazil)
    public static function toCamelCase($str, $capitalise_first_char = false) {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $str);
    }

    public static function varFancyDump($var) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    public static function strEndsWith($string, $target) {
        return $string === "" || substr($string, -strlen($target)) === $target;
    }

    public static function strStartsWith($string, $target) {
        return $string === "" || substr($string, 0, strlen($target)) === $target;
    }

    public static function strFixPath($path) {
        $retVal = str_replace('\\', DIRECTORY_SEPARATOR, str_replace('/', DIRECTORY_SEPARATOR, $path));
        return $retVal;
    }

    public static function strWebPath($path) {
        $retVal = str_replace('\\', DIRECTORY_SEPARATOR, $path);
        return $retVal;
    }

}
