<?php

namespace Karucha\Access\Session;

use Brickify\Answer\BrickifyAnswer;
use Brickify\Database\DBBrick;
use Brickify\ResultSet\ResultSetSlot;
use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\DAOs\ServerConfDAO;
use Karucha\DAOs\UserDAO;
use Karucha\Keys\KaruchaKeys;
use Karucha\Rules\NoSpecialCharsRule;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\UniqueUsernameRule;
use Karucha\Rules\StringLengthFilter;
use Karucha\Session\PasswordAuthentication;
use Karucha\Tables\ServerConfTable;
use Karucha\Tables\UserCredentialsTable;
use Karucha\Tables\UsersTable;
use PDO;
use RoastPHP\Util\Sql\SqlCriteria;
use RoastPHP\Util\Sql\SqlFilter;

class AddUserService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'ADD USER must be triggered by a HTTP POST request')
            ),
            'parameters' => array(
                'name' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new StringLengthFilter(3, 30), 'NAME must have between 3 and 30 characters')
                    )
                ),
                'nickname' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new StringLengthFilter(3, 20), 'NICKNAME must have between 3 and 20 characters')
                    )
                ),
                'email' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new StringLengthFilter(6, 50), 'USERNAME must have between 6 and 50 characters')
                    )
                ),
                'username' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new StringLengthFilter(6, 22), 'USERNAME must have between 6 and 22 characters'),
                        new ServiceFilter(new UniqueUsernameRule(), 'USERNAME already exists')
                    )
                ),
                'password' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new NoSpecialCharsRule(), 'PASSWORD only allows characters and numbers')
                    )
                ),
                'server_key' => array(
                    'required' => false
                )
            )
        );
    }

    private function unlockServer(PDO $dbDriver, $givenPassword) {
        $serverConfDAO = new ServerConfDAO($dbDriver);

        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(ServerConfTable::COLUMN_KEY, '=', KaruchaKeys::SERVER_PASSWORD));

        $configs = $serverConfDAO->get($criteria);

        if (empty($configs)) {
            return true;
        } else if (is_null($givenPassword)) {
            return false;
        } else {
            $serverRS = $configs[0];
            $serverPassword = $serverRS->getField(ServerConfTable::COLUMN_VALUE);
            return $givenPassword == $serverPassword;
        }
    }

    protected function onSuccess($parameters) {
        $answer = new BrickifyAnswer();
        $dbDriver = DBBrick::getDriver();

        $givenPassword = (isset($parameters['server_key']) ? $parameters['server_key'] : null);
        if ($this->unlockServer($dbDriver, $givenPassword)) {
            $userDAO = new UserDAO($dbDriver);

            $userStructure = array(
                UsersTable::COLUMN_NAME => $parameters['name'],
                UsersTable::COLUMN_NICKNAME => $parameters['nickname'],
                UsersTable::COLUMN_EMAIL => $parameters['email']
            );

            $rsUser = new ResultSetSlot($userStructure);
            $userDAO->save($rsUser);

            $userId = $rsUser->getField(UsersTable::COLUMN_ID);

            if (!is_null($userId) && $userId > 0) {
                $credentialsStructure = array(
                    UserCredentialsTable::COLUMN_USER_ID => $userId,
                    UserCredentialsTable::COLUMN_USERNAME => $parameters['username'],
                    UserCredentialsTable::COLUMN_PASSWORD => $parameters['password'],
                    UserCredentialsTable::COLUMN_PERMISSION => 1,
                    UserCredentialsTable::COLUMN_ITERATIONS => 0
                );

                $passwordAuth = new PasswordAuthentication($dbDriver);
                $affected = $passwordAuth->rehash(new ResultSetSlot($credentialsStructure), $parameters['password']);

                if ($affected == 1) {
                    $answer->setStatus(1);
                    $answer->setMessages(array('User has been saved'));
                    $answer->setContents(array());
                }
            } else {
                $answer->setStatus(0);
                $answer->addMessage('Not possible to save the user profile');
                $answer->setContents(array());
            }
        } else {
            $answer->setStatus(0);
            $answer->addMessage('This server is private. Please give a valid SERVER KEY');
            $answer->setContents(array());
        }
        
        return $answer;
    }

}
