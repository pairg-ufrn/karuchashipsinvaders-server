<?php

namespace RoastPHP\Util\Encrypt;

class Base64Reverse implements EncryptedData, DecryptedData {

    private $recursionFactor = 0;

    public function __construct($recursionFactor = 0) {
        $this->recursionFactor = $recursionFactor;
    }

    public function getRecursionFactor() {
        return $this->recursionFactor;
    }

    public function encrypt($data) {
        $str = $data;
        for ($i = 0; $i < ($this->recursionFactor + 1); $i++) {
            $str = strrev(base64_encode($str));
        }
        return $str;
    }

    public function decrypt($data) {
        $str = $data;
        for ($i = 0; $i < ($this->recursionFactor + 1); $i++) {
            $str = base64_decode(strrev($str));
        }
        return $str;
    }

}
