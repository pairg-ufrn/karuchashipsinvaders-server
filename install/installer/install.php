<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set("Brazil/East");

require_once dirname(__FILE__).'/KaruchaInstaller.class.php';
require_once dirname(__FILE__).'/../../lib/autoload.php';

$installer = new KaruchaInstaller();
$installer->install();