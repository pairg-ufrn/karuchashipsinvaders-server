<?php

use Brickify\ConfigFiles\BrickifySafeFile;
use Brickify\ResultSet\ResultSetSlot;
use Karucha\DAOs\ServerConfDAO;
use Karucha\DAOs\UserDAO;
use Karucha\Keys\KaruchaKeys;
use Karucha\Session\PasswordAuthentication;
use Karucha\Tables\ServerConfTable;
use Karucha\Tables\UserCredentialsTable;
use Karucha\Tables\UsersTable;

class KaruchaInstaller {

    private $installMethod;

    public function install() {
        $this->installMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        $this->step1();
    }

    // Verifica se podemos escrever na localidade selecionada
    private function step1() {
        $safeFile = new BrickifySafeFile('database');
        if ($safeFile->isWritable()) {
            $this->step2($safeFile);
        } else {
            die(
                    'Impossível escrever o arquivo de configuração.\n '
                    . 'Verifique as opções de escrita para a pasta "' . BrickifySafeFile::DEFAULT_CONF_DIR . '"'
            );
        }
    }

    // Conectando ao banco de dados
    private function step2($safeFile) {
        $mySqlHost = filter_input(INPUT_POST, 'mysql_host');
        $mySqlUser = filter_input(INPUT_POST, 'mysql_user');
        $mySqlPass = filter_input(INPUT_POST, 'mysql_pass');
        $mySqlPort = filter_input(INPUT_POST, 'mysql_port');

        $newDbName = filter_input(INPUT_POST, 'mysql_database');
        $pdo = $this->createPDO($mySqlHost, $mySqlUser, $mySqlPass, $mySqlPort);
        if (!is_null($pdo)) {
            //$this->step3($pdo, $newDbName, $safeFile);
            $this->step4($pdo, $newDbName, $safeFile);
        } else {
            die('Não foi possível conectar-se ao banco de dados com as informações dadas. Verifique-as e tente novamente.');
        }
    }

    private function createPDO($host, $user, $pass, $port) {
        $pdoDNS = "mysql:host={$host};port={$port}";
        try {
            $dbDriver = new PDO($pdoDNS, $user, $pass);
            $dbDriver->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbDriver;
        } catch (Exception $ex) {
            return null;
        }
    }

    // Criando o banco de dados (Ou tentando, se este já existe)
    private function createDatabase(PDO $pdo, $databaseName, $safeFile) {
        $created = 0;
        $query = "CREATE DATABASE {$databaseName} ";
        $query .= "DEFAULT CHARACTER SET utf8 ";
        $query .= "DEFAULT COLLATE utf8_general_ci; ";

        try {
            $pdo->exec($query);
            $created += 1;
        } catch (Exception $ex) {
            $created += 2;
        }
        
        return $created;
    }

    // Verificando se o banco de dados foi salvo
    private function step4(PDO $pdo, $databaseName, $safeFile) {
        try {
            $createdSituation = $this->createDatabase($pdo, $databaseName, $safeFile);
            $pdo->exec('USE ' . $databaseName);
            $this->step5($pdo, $safeFile);
        } catch (Exception $ex) {
            if($createdSituation == 1){
                $message = 'Não foi possível estabelecer a ligação com o banco de dados '. $databaseName .'. Selecione outro nome e tente novamente.';
            }else if($createdSituation == 2){
                $message = 'O banco de dados ' . $databaseName . ' não pode ser utilizado pelo usuário atual. Verifique os privilégios do usuário e tente novamente.';
            }
            die($message);
        }
    }

    // Criando as estruturas no banco de dados
    private function step5(PDO $pdo, $safeFile) {
        $scriptFile = dirname(__FILE__) . "/../database/database_structure.sql";
        $sqlInstruction = file_get_contents($scriptFile);

        $pdo->exec($sqlInstruction);
        $this->step6($pdo, $safeFile);
    }
    
    // Adicionando o usuário requisitado
    private function step6(PDO $pdo, $safeFile) {
        $name = $nick = filter_input(INPUT_POST, 'nu_name');
        $email = filter_input(INPUT_POST, 'nu_email');
        $user = filter_input(INPUT_POST, 'nu_username');
        $pass = filter_input(INPUT_POST, 'nu_password');

        $isPrivate = filter_input(INPUT_POST, 'server_config') == 'private';

        $userDAO = new UserDAO($pdo);
        $userStructure = array(
            UsersTable::COLUMN_NAME => $name,
            UsersTable::COLUMN_NICKNAME => $nick,
            UsersTable::COLUMN_EMAIL => $email
        );
        $rsUser = new ResultSetSlot($userStructure);
        $userDAO->save($rsUser);
        $userId = $rsUser->getField(UsersTable::COLUMN_ID);

        $credentialsStructure = array(
            UserCredentialsTable::COLUMN_USER_ID => $userId,
            UserCredentialsTable::COLUMN_USERNAME => $user,
            UserCredentialsTable::COLUMN_PASSWORD => $pass,
            UserCredentialsTable::COLUMN_PERMISSION => 0,
            UserCredentialsTable::COLUMN_ITERATIONS => 0
        );
        $passwordAuth = new PasswordAuthentication($pdo);
        $passwordAuth->rehash(new ResultSetSlot($credentialsStructure), $pass);

        if ($isPrivate) {
            $this->alternativeFlux6($pdo);
        }
        
        $this->step7($pdo, $safeFile);
    }

    private function alternativeFlux6(PDO $pdo) {
        $serverPassword = filter_input(INPUT_POST, 'server_password');
        
        $systemConfDAO = new ServerConfDAO($pdo);
        $confStructure = array(
            ServerConfTable::COLUMN_KEY => KaruchaKeys::SERVER_PASSWORD,
            ServerConfTable::COLUMN_VALUE => sha1($serverPassword)
        );

        $rsConf = new ResultSetSlot($confStructure);
        $systemConfDAO->save($rsConf);
    }

    private function step7($pdo, BrickifySafeFile $safeFile){
        $mySqlHost = filter_input(INPUT_POST, 'mysql_host');
        $mySqlUser = filter_input(INPUT_POST, 'mysql_user');
        $mySqlPass = filter_input(INPUT_POST, 'mysql_pass');
        $mySqlPort = filter_input(INPUT_POST, 'mysql_port');
        $newDbName = filter_input(INPUT_POST, 'mysql_database');
        
        $newDbFile = array(
            'db_host' => $mySqlHost,
            'db_user' => $mySqlUser,
            'db_pass' => $mySqlPass,
            'db_name' => $newDbName,
            'db_port' => $mySqlPort,
            'db_type' => 'mysql'
        );
        
        $safeFile->setContent($newDbFile);
        $safeFile->writeToFile();
        
        $this->step8();
    }
    
    private function step8(){
        header('Location: ../../index.php');
    }
}
