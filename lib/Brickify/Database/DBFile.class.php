<?php

namespace Brickify\Database;

use Brickify\ConfigFiles\BrickifySafeFile;

class DBFile {

    private $dbHost;
    private $dbUser;
    private $dbPass;
    private $dbName;
    private $dbPort;
    private $dbType;

    public function __construct($pointer) {
        if (is_array($pointer)) {
            $contents = $pointer;
        } else if(is_string ($pointer)){
            $safeFile = new BrickifySafeFile($pointer);
            $contents = $safeFile->getContent();
        }

        $this->dbHost = isset($contents['db_host']) ? $contents['db_host'] : null;
        $this->dbUser = isset($contents['db_user']) ? $contents['db_user'] : null;
        $this->dbPass = isset($contents['db_pass']) ? $contents['db_pass'] : null;
        $this->dbName = isset($contents['db_name']) ? $contents['db_name'] : null;
        $this->dbPort = isset($contents['db_port']) ? $contents['db_port'] : null;
        $this->dbType = isset($contents['db_type']) ? $contents['db_type'] : null;
    }

    public function getDbHost() {
        return $this->dbHost;
    }

    public function getDbUser() {
        return $this->dbUser;
    }

    public function getDbPass() {
        return $this->dbPass;
    }

    public function getDbName() {
        return $this->dbName;
    }

    public function getDbPort() {
        return $this->dbPort;
    }

    public function getDbType() {
        return $this->dbType;
    }

    public function isGeneric() {
        $dbUser = $this->getDbUser();
        $dbPass = $this->getDbPass();
        $dbName = $this->getDbName();
        return (empty($dbUser) || empty($dbPass) || empty($dbName));
    }

}
