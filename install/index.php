<?php

use Brickify\Database\DBBrick;

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once dirname(__FILE__).'/../lib/autoload.php';
require_once dirname(__FILE__).'/../conf/AppConfig.inc.php';

$connected = !is_null(DBBrick::getDriver());
if($connected){
    header("Location: ../index.php");
}else{
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>KaruchaShipsInvaders - Install Server</title>

        <script src="res/dist/jquery-1.11.0.min.js"></script>

        <!-- Bootstrap core CSS -->
        <link href="res/dist/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="res/css/install.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="../../assets/js/html5shiv.js"></script>
          <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">
            <center>
                <img src="../res/img/icone.png" width="264" height="264"/>
                <br/>
            </center>
            <form class="form-signin" class="form-horizontal" action="installer/install.php" method="POST">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">MySQL Authentication</legend>
                    <div class="form-group">
                        <label for="mysql_host" class="col-sm-2 control-label">Host</label>
                        <div class="col-sm-10">
                            <input ind="mysql_host" type="text" name="mysql_host" class="form-control" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mysql_user" class="col-sm-2 control-label">User</label>
                        <div class="col-sm-10">
                            <input id="mysql_user" type="text" name="mysql_user" class="form-control" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mysql_pass" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input id="mysql_pass" type="password" name="mysql_pass" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mysql_port" class="col-sm-2 control-label">Port</label>
                        <div class="col-sm-10">
                            <input id="mysql_port" type="text" name="mysql_port" class="form-control" value="3306" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mysql_database" class="col-sm-2 control-label">Database</label>
                        <div class="col-sm-10">
                            <input id="mysql_database" type="text" name="mysql_database" class="form-control" autofocus>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Admin User</legend>

                    <div class="form-group">
                        <label for="nu_username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" name="nu_username" class="form-control" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nu_password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="nu_password" class="form-control" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nu_name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input id="nu_name" type="text" name="nu_name" class="form-control" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nu_email" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10">
                            <input id="nu_email" type="text" name="nu_email" class="form-control">
                        </div>
                    </div>
                </fieldset>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Server Configuration</legend>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="server_config" id="public-server" value="public" checked>
                                Public server
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="server_config" id="private-server" value="private">
                                Private server
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="server_password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input id="server_password" type="text" name="server_password" class="form-control" autofocus disabled>
                        </div>
                    </div>

                </fieldset>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Install</button>
            </form>
        </div> <!-- /container -->
        <script>
            $(function() {
                $('#private-server').click(function() {
                    $('#server_password').prop('disabled', false);
                });

                $('#public-server').click(function() {
                    $('#server_password').prop('value', '');
                    $('#server_password').prop('disabled', true);
                });
            });
        </script>
    </body>
</html>
<?php
}