<?php

namespace Karucha\Rules;

use Brickify\Rules\DataRule;

class StringLengthFilter implements DataRule{
    
    private $minLength;
    private $maxLength;
    
    public function __construct($minLength = 4, $maxLength = 34) {
        $this->minLength = $minLength;
        $this->maxLength = $maxLength;
    }

    public function isValidRule($data = null) {
        $strLength = strlen($data);
        return ($this->minLength <= $strLength && $strLength <= $this->maxLength);
    }

}