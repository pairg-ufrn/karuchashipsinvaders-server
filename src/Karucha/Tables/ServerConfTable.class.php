<?php

namespace Karucha\Tables;

class ServerConfTable{
    
    const TABLE_NAME = 'server_conf';
    
    const COLUMN_ID = 'id';
    const COLUMN_KEY = 'conf_key';
    const COLUMN_VALUE = 'value';
    const COLUMN_CREATED = 'created';
}