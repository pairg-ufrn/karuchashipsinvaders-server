<?php

namespace Karucha\Session;

use Brickify\ResultSet\ResultSetSlot;
use Karucha\DAOs\UserCredentialsDAO;
use Karucha\Tables\UserCredentialsTable;
use PDO;
use Phpass\Hash;
use Phpass\Hash\Adapter\Sha1Crypt;
use RoastPHP\Util\Sql\Query\SelectQuery;
use RoastPHP\Util\Sql\SqlCriteria;
use RoastPHP\Util\Sql\SqlFilter;

class PasswordAuthentication {

    const MIN_ITERATIONS = 1000;
    const MAX_ITERATIONS = 15000;
    
    private $databaseDriver;

    public function __construct(PDO $databaseDriver) {
        $this->databaseDriver = $databaseDriver;
    }
    
    public function authenticate($username, $password) {
        $evaluatedCredentials = null;
        
        $findUserQuery = new SelectQuery();
        $findUserQuery->addColumn("*");
        $findUserQuery->setEntity(UserCredentialsTable::TABLE_NAME);

        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(UserCredentialsTable::COLUMN_USERNAME, '=', $username));
        $findUserQuery->setCriteria($criteria);

        $credentials = $this->getCredentials($findUserQuery);
        if (!is_null($credentials)) {
            if ($this->isValidLogin($credentials, $password)) {
                $evaluatedCredentials = $credentials;
                $this->rehash($credentials, $password);
            }
        }

        return $evaluatedCredentials;
    }

    private function getCredentials(SelectQuery $query) {
        $statement = $this->databaseDriver->query($query->getInstruction());
        if ($statement->rowCount() == 1) {
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $credentials = null;
            while ($resultSet = $statement->fetch()) {
                $credentials = new ResultSetSlot($resultSet);
            }
            return $credentials;
        } else {
            return null;
        }
    }
    
    private function isValidLogin(ResultSetSlot $credentials, $password) {
        $phpassHash = $this->getPhpassHash($credentials->getField(UserCredentialsTable::COLUMN_ITERATIONS));
        if($phpassHash->checkPassword($password, $credentials->getField(UserCredentialsTable::COLUMN_PASSWORD))){
            return true;
        }else{
            return false;
        }
    }

    public function rehash(ResultSetSlot $credentials, $password){
        $iterations = rand(self::MIN_ITERATIONS, self::MAX_ITERATIONS);
        $credentials->setField(UserCredentialsTable::COLUMN_ITERATIONS, $iterations);
        
        $phpassHash = $this->getPhpassHash($iterations);
        $newPass = $phpassHash->hashPassword($password);
        $credentials->setField(UserCredentialsTable::COLUMN_PASSWORD, $newPass);
        
        $credentialsDAO = new UserCredentialsDAO($this->databaseDriver);
        return $credentialsDAO->save($credentials);
    }
    
    private function getPhpassHash($iterations){
        $adapter = new Sha1Crypt(array(
            'iterationCount' => $iterations
        ));
        
        $phpassHash = new Hash($adapter);
        
        return $phpassHash;
    }
}