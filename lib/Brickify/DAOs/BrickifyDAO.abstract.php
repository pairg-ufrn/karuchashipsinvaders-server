<?php

namespace Brickify\DAOs;

use Brickify\ResultSet\ResultSetSlot;
use PDO;
use RoastPHP\Util\Sql\Query\DeleteQuery;
use RoastPHP\Util\Sql\Query\InsertQuery;
use RoastPHP\Util\Sql\Query\SelectQuery;
use RoastPHP\Util\Sql\Query\SqlInstruction;
use RoastPHP\Util\Sql\Query\UpdateQuery;
use RoastPHP\Util\Sql\SqlCriteria;
use RoastPHP\Util\Sql\SqlFilter;
use RoastPHP\Util\StrOpers;



abstract class BrickifyDAO {

    // The default primary key field. Once set, all tables must have this field
    const DEFAULT_PRIMARY_KEY = 'id';
    const DEFAULT_CREATED = 'created';
    const DEFAULT_MODIFIED = 'modified';

    protected $driver;
    protected $tableName;
    protected $saveCreated;
    protected $saveModified;

    public function __construct($tableName, PDO $driver, $saveCreated = true, $saveModified = true) {
        $this->driver = $driver;

        $this->tableName = $tableName;
        $this->saveCreated = $saveCreated;
        $this->saveModified = $saveModified;
    }

    private function parseResultSet($resultSet) {
        return new ResultSetSlot($resultSet);
    }

    public function get($pointer) {
        if (is_numeric($pointer)) {
            $singleRecord = true;
            $criteria = new SqlCriteria();
            $criteria->add(new SqlFilter(self::DEFAULT_PRIMARY_KEY, '=', $pointer));
        } else {
            $singleRecord = false;
            $criteria = $pointer;
        }

        $selected = $this->select($criteria);
        if ($singleRecord) {
            if (!empty($selected)) {
                return $selected[0];
            } else {
                return null;
            }
        }

        return $selected;
    }

    public function select($criteria) {
        $query = new SelectQuery();
        $query->addColumn('*');
        $query->setEntity($this->tableName);
        if(!is_null($criteria)){
            $query->setCriteria($criteria);
        }

        $statement = $this->driver->query($query->getInstruction());
        $results = array();

        while ($resultSet = $statement->fetch(PDO::FETCH_ASSOC)) {
            $results[] = $this->parseResultSet($resultSet);
        }

        return $results;
    }

    public function save(ResultSetSlot &$resultSet) {
        $affected = 0;
        if (is_null($resultSet->getField(self::DEFAULT_PRIMARY_KEY))) {
            $affected = $this->insert($resultSet);
        } else {
            $affected = $this->update($resultSet);
        }
        
        return $affected;
    }

    public function delete(ResultSetSlot &$resultSet) {
        $query = new DeleteQuery();
        $query->setEntity($this->tableName);

        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(self::DEFAULT_PRIMARY_KEY, '=', $resultSet->getField(self::DEFAULT_PRIMARY_KEY)));
        $query->setCriteria($criteria);

        $affected = $this->driver->exec($query->getInstruction());
        if ($affected > 0) {
            $resultSet = null;
        }
    }

    private function insert(ResultSetSlot &$resultSet) {
        $query = new InsertQuery();
        $query->setEntity($this->tableName);

        $resultSetArray = $resultSet->getResultSet();
        foreach ($resultSetArray as $column => $value) {
            $columnName = StrOpers::fromCamelCase($column);
            if (!in_array($columnName, $this->exceptionFields())) {
                $query->setRowData($column, $value);
            }
        }

        if ($this->saveCreated) {
            $this->setCreated($resultSet, $query);
        }
        
        $affected = $this->driver->exec($query->getInstruction());
        $resultSet->setField(self::DEFAULT_PRIMARY_KEY, $this->driver->lastInsertId());
        
        return $affected;
    }

    private function update(ResultSetSlot &$resultSet) {
        $query = new UpdateQuery();
        $query->setEntity($this->tableName);

        foreach ($resultSet->getResultSet() as $columnName => $value) {
            if (!in_array($columnName, $this->exceptionFields()) || !is_numeric($columnName)) {
                $query->setRowData($columnName, $value);
            }
        }

        if ($this->saveModified) {
            $this->setModified($resultSet, $query);
        }

        $criteria = new SqlCriteria();
        $criteria->add(new SqlFilter(self::DEFAULT_PRIMARY_KEY, '=', $resultSet->getField(self::DEFAULT_PRIMARY_KEY)));
        $query->setCriteria($criteria);

        return $this->driver->exec($query->getInstruction());
    }

    private function setCreated(ResultSetSlot &$resultSet, SqlInstruction &$query) {
        $created = $this->now();
        $query->setRowData(self::DEFAULT_CREATED, $created);
        $resultSet->setField(self::DEFAULT_CREATED, $created);
    }

    private function setModified(ResultSetSlot &$resultSet, SqlInstruction &$query) {
        $modified = $this->now();
        $query->setRowData(self::DEFAULT_MODIFIED, $modified);
        $resultSet->setField(self::DEFAULT_MODIFIED, $modified);
    }

    private function now() {
        return date('Y-m-d H:i:s');
    }

    private function exceptionFields() {
        return array(
            self::DEFAULT_PRIMARY_KEY,
            self::DEFAULT_CREATED,
            self::DEFAULT_MODIFIED
        );
    }

}
