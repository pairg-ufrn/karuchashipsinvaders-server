<?php

use Brickify\Access\LauncherBrick;

error_reporting(E_ALL);
ini_set('display_errors', 1);

date_default_timezone_set('Brazil/East');

//////////////
// Requires //
//////////////
require_once 'lib' . DIRECTORY_SEPARATOR . 'autoload.php';


/////////////
// Routing //
/////////////
// TODO: Fix and filter
$parameters = array();

if (!empty($_GET)) {
    $parameters = $_GET;
} else if (!empty($_POST)) {
    $parameters = $_POST;
}

///////////////
// Launching //
///////////////
$launcher = new LauncherBrick();

$service = $launcher->launch($parameters);
$answer = $service->call($parameters);
$answer->pack();