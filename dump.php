<?php

use Brickify\Access\LauncherBrick;

error_reporting(E_ALL);
ini_set('display_errors', 1);

date_default_timezone_set('Brazil/East');

//////////////
// Requires //
//////////////
require_once 'lib' . DIRECTORY_SEPARATOR . 'autoload.php';

/////////////
// Routing //
/////////////
// TODO: Fix and filter
$parameters = array();
if ($_GET) {
    $parameters = $_GET;
} else if ($_POST) {
    $parameters = $_POST;
}

///////////////
// Launching //
///////////////
if (!empty($parameters)) {
    $launcher = new LauncherBrick();
    $service = $launcher->launch($parameters);
    $parameters = $service->dumpParameters();
} else {
    $allServices = brickify_app_services();
    $allServiceNames = array_keys($allServices);
    $parameters = $allServiceNames;
}

foreach($parameters as $parameter){
    echo $parameter . '<br/>';
}
