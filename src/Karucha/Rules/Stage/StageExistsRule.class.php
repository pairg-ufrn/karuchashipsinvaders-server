<?php

namespace Karucha\Rules\Stage;

use Brickify\Database\DBBrick;
use Brickify\Rules\DataRule;
use Karucha\DAOs\StageDAO;

class StageExistsRule implements DataRule{
    
    public function isValidRule($data = null) {
        $driver = DBBrick::getDriver();
        $dao = new StageDAO($driver);
        
        return (!is_null($dao->get($data)));
    }

}
