<?php

namespace Brickify\Access;

use Brickify\Services\BrickifyEmptyService;

class LauncherBrick{
    
    const METHOD_KEY = 'method';
    
    public function launch($parameters = null){
        $methodInvoker = isset($parameters[self::METHOD_KEY]) ? $parameters[self::METHOD_KEY] : null;
        $appServices = brickify_app_services();
        
        if(isset($appServices[$methodInvoker])){
            $service = $appServices[$methodInvoker];
        }else{
            $service = new BrickifyEmptyService();
        }
        
        unset($appServices);
        return $service;
    }
    
}