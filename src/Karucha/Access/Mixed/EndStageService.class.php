<?php

namespace Karucha\Access\Mixed;

use Brickify\Services\BrickifyService;
use Brickify\Services\ServiceFilter;
use Karucha\Rules\IsValidJsonFilter;
use Karucha\Rules\PostRequestRule;
use Karucha\Rules\Session\SameClientRule;
use Karucha\Rules\Session\ValidSessionIdRule;

class EndStageService extends BrickifyService {

    protected function serviceConditions() {
        return array(
            'operation' => array(
                new ServiceFilter(new PostRequestRule(), 'End Stage must be triggered by a HTTP POST request')
            ),
            'parameters' => array(
                'stage_json' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new IsValidJsonFilter(), 'STAGE JSON is not a valid JSON string')
                    )
                ),
                'sess_id' => array(
                    'required' => true,
                    'filters' => array(
                        new ServiceFilter(new ValidSessionIdRule(), 'Invalid Session'),
                        new ServiceFilter(new SameClientRule(), 'Invalid client'),
                    )
                )
            )
        );
    }

    protected function onSuccess($parameters) {
        $stageJson = json_decode($parameters['stage_json'], true);
        $stageJson['sess_id'] = $parameters['sess_id'];

        $serviceLogic = new EndStageLogic();
        $answer = $serviceLogic->call($stageJson);
        
        return $answer;
    }

}
